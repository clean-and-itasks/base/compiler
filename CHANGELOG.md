# Changelog

#### v4.0.2

- Fix: prevent a compiler crash if a partially applied abstract new type is used in the type of a constructor.
- Fix: generated code for matching a constructor containing a (class constraint) dictionary.

#### v4.0.1

- Fix: store position of the default of an explicit case in `case_default_pos`.
- Fix: prevent possible compiler crash for an (erroneous) type context with a free type variable.
- Fix: generated `.r` annotations for node entries.

## v4.0.0

- Change: only import generic cases/derives that are imported by an explicit
  import or import of the whole module, instead of importing all generic
  cases/derives in all (partially) imported definition modules.
- Feature: add specifying types for expressions with syntax: `(expr :: type)`,
  uniqueness attribute equalities or type contexts are not allowed. The
  specified type may be more general, uniqueness attributes that are required
  because of propagation are not added by the compiler, so for example `[*Int]`
  should be `*[*Int]`.
- Feature: add option to generate `.r` annotations for entrypoints, which
  indicate the number and types of A/B-stack arguments returned by the
  entrypoint.

### v3.3.0

- Feature: allow explicit imports of generic case/instance definitions, syntax:
  `derive generic_function_name types` or `derive class class_name types`, types
  are not separated by white space, not commas, for example: `from GenEq import
  derive gEq OBJECT CONS`. Currently, the compiler just checks such imports, all
  such definitions are still imported anyway. This is added to allow for
  code backwards compatible to version `3` of the compiler.
- Fix: remove error `no generic cases for this kind` that occurs if a generic
  context is used with a kind for which no generic cases are defined. The
  generic contexts are now converted before the classes for the generic
  instances are created so that all these classes can be created. The generated
  generic bimap contexts are now immediately converted when they are created.

### v3.2.0

- Feature: add flag `-oltr` to enable lazy tuple recursion optimisation.
- Enhancement: improve code generation for a constructor and (unevaluated)
  record update in a lazy context for constructors with a single unboxed record
  argument.
- Enhancement: improve generated code for a record update in a lazy context
  that updates (a) field(s) with a basic type constant.
- Enhancement: optimisation, thunk lift record updates in a lazy context
  updating (a) strict field(s) with function call(s) expressions in a lazy
  context; if the original record was unboxed pass this partial (not updated
  part) record unboxed.
- Enhancement: optimisation, thunk lift record expressions in a lazy context
  with (a) strict field value(s) which are function call(s) in a lazy context.
- Enhancement: optimisation, extend thunk lifting of record updates in a lazy
  context updating (a) strict field(s).
- Fix: prevent compile time error for a newtype of a higher kinded type
  variable application.
- Fix: prevent compiler crash when parsing a record update that specifies the
  record and has a selector that also specifies the record and one or both
  records are qualified with a module name.
- Fix: fix signs and propagation of arguments of mutually recursive type
  definitions: convert type argument numbers to group variable numbers.
- Fix: don't allow fields without values in expressions, only in patterns, for
  example not `{R | x}` as shorthand for `{R | x=x}`.
- Fix: if a field is qualified and only imported by a qualified import of the
  whole module it was necessary to also explicitly import this field (and
  record). This is no longer necessary if the record is also specified, for
  example: `r.'m'.R.'m'.f`, `r.R.'m'.f`, `{R | r & 'm'.f=0}`, or
  `{R | 'm'.f=0}`.
- Fix: parsing without the layout rule; previous version could sometimes
  require extra brackets.

#### v3.1.1

- Fix: fix type checking of updates of arrays of unique coercible elements.
- Fix: reject `case` expressions without alternatives in parser.
- Fix: fix sign classification of type parameters after the first of type
  definitions.
- Misc: delay expanding newtypes in the type checker.

### v3.1.0

- Enhancement: allow patterns after qualified constructors in `=:` expressions
  without parentheses (e.g. `a=:'M'.C 0`); already allowed for constructors in
  the global namespace.
- Enhancement: optimize calls to generic functions with an implementation
  exported in a definition module by creating a local copy (except for generic
  representation types, e.g. `PAIR`).
- Enhancement: add unfolding of functions yielding a character, integer or real
  constant in (trivial) fusion.
- Fix: fix bug in generic fusion for active trivial boolean cases on a case
  path with a function argument.
- Fix: report an error for tuples with more than 32 elements.
- Fix: improve checking of required imports of module StdStrictLists.
- Fix: prevent compiler crash when compiling macros such as `.` imported from a
  definition module.
- Fix: disallow unique attributes that propagate to the root in CAF types.
- Fix: fix parsing of qualified non-alphanumeric identifiers with a module name
  of length 1 (e.g. `'L'. ++`).
- Fix: fix parsing of record patterns without definitions; don't allow
  definitions.
- Fix: fix compiler crash for match functions generated for irrefutable unboxed
  list patterns when the elements are not in a strict context.
- Fix: fix reporting of uniqueness type coercion errors.

#### v3.0.3

- Fix: substitute type variables and attributes when expanding a newtype with a
  universal quantifier.
- Fix: fix replacement of an explicit case by a case alternative with a strict
  let.
- Fix: fix bug that prevented unboxing arguments of some closures.

#### v3.0.2

- Enhancement: add optimisation to generic fusion to prevent generating some
  small functions with many arguments that may cause fusion to stop because of
  the 32 arguments limit. Instead generate larger functions with fewer
  arguments by inlining function calls.
- Enhancement: speed up and reduce memory usage of compilation with generic
  fusion. Instead of generating a new trivial function and inlining the
  function later do this immediately (without generating a new function) in
  some cases.
- Fix: the Windows version of `lib-compiler` now distributes `backend.dll` in
  `misc/dll/backend.dll` instead of `exe/backend.dll` to avoid a name clash
  with `base-compiler`.

#### v3.0.1

- Fix: prevent compiler crash when explicitly importing a macro using a new
  type pattern match.

## v3.0

- Feature: add `{ :}`, `{! :}`, `{# :}`, and `{32# :}` array types which
  reserve up to the next power of 2 of memory. This uses the ABC instructions
  `create_arrayp2` and `create_arrayp2_`.
- Feature: add functional dependencies (backported from the iTasks compiler).
- Feature: add `import qualified .. as ..` (backported from the iTasks
  compiler).
- Feature: add `binumap` (backported from the iTasks compiler).
- Fix: fix printing of higher-order array types.
- Fix: prevent stack overflow when compiling very large function types.

#### v2.0.1

- Chore: allow `base-stdenv` ^2.0.

## v2.0

- Enhancement: use ABC instructions `select_nc` and `update_nc` for array
  updates and selects when indexes do not need to be checked (e.g. in array
  comprehensions).
- Enhancement: add names of comprehensions to generated identifiers in
  patterns.
- Fix: bug in generic fusion causing incorrect functions to be generated.

## v1.0

First tagged version.
