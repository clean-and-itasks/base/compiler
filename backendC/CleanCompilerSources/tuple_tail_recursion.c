
#define for_l(v,l,n) for(v=(l);v!=NULL;v=v->n)
#define for_li(v,i,l,n) for(v=(l),i=0;v!=NULL;v=v->n,++i)

#include "compiledefines.h"
#include "types.t"
#include "system.h"
#include "settings.h"
#include "syntaxtr.t"
#include "sizes.h"
#include "comsupport.h"
#include "codegen_types.h"
#include "tuple_tail_recursion.h"

#if TAIL_CALL_MODULO_TUPLE_CONS_OPTIMIZATION

static int is_tuple_tail_call_modulo_cons_defs (NodeP root_node,NodeDefP defs,NodeIdP tuple_call_node_id_p,unsigned long *same_select_vector_p)
{
	NodeDefP last_node_def_p;

	last_node_def_p=defs;
	while (last_node_def_p->def_next!=NULL && last_node_def_p->def_id!=tuple_call_node_id_p)
		last_node_def_p=last_node_def_p->def_next;

	if (last_node_def_p->def_next==NULL && last_node_def_p->def_id==tuple_call_node_id_p &&
		last_node_def_p->def_node->node_kind==TupleSelectorsNode &&
		last_node_def_p->def_node->node_arguments->arg_node->node_kind==NodeIdNode)
	{
		ArgP tuple_element_p,function_result_tuple_element_p;
		ArgP function_result_tuple_elements_a[MaxNodeArity],*function_result_tuple_elements_p;
		unsigned long same_select_vector;
		int n;

		function_result_tuple_elements_p=&function_result_tuple_elements_a[0];
		for_l (function_result_tuple_element_p,last_node_def_p->def_node->node_arguments,arg_next)
			*function_result_tuple_elements_p++ = function_result_tuple_element_p;

		same_select_vector=0;

		for_li (tuple_element_p,n,root_node->node_arguments,arg_next){
			NodeP node_p;

			node_p=tuple_element_p->arg_node;
			--function_result_tuple_elements_p;

			if (node_p->node_kind==NodeIdNode && node_p->node_node_id->nid_refcount>0
				&& node_p->node_node_id==(*function_result_tuple_elements_p)->arg_node->node_node_id)
			{
				same_select_vector |= (1<<n);
			}
		}

		*same_select_vector_p &= same_select_vector;
		return 1;
	}

	return 0;
}

int roots_are_tuples_or_calls_to_this_function_and_compute_same_select_vector (NodeP node_p,NodeDefP node_defs,SymbDef function_sdef_p,unsigned long *same_select_vector_p,int *has_tuple_tail_call_p)
{
	switch (node_p->node_kind){
		case SwitchNode:
		{
			ArgP arg_p;

			for_l (arg_p,node_p->node_arguments,arg_next){
				if (!roots_are_tuples_or_calls_to_this_function_and_compute_same_select_vector (arg_p->arg_node->node_arguments->arg_node,arg_p->arg_node->node_node_defs,function_sdef_p,same_select_vector_p,has_tuple_tail_call_p))
					return 0;
			}

			return 1;
		}
		case PushNode:
			return roots_are_tuples_or_calls_to_this_function_and_compute_same_select_vector (node_p->node_arguments->arg_next->arg_node,node_defs,function_sdef_p,same_select_vector_p,has_tuple_tail_call_p);
		case GuardNode:
		{
			while (node_p->node_kind==GuardNode){
				if (!roots_are_tuples_or_calls_to_this_function_and_compute_same_select_vector (node_p->node_arguments->arg_node,node_defs,function_sdef_p,same_select_vector_p,has_tuple_tail_call_p))
					return 0;

				node_defs=node_p->node_node_defs;
				node_p=node_p->node_arguments->arg_next->arg_node;
			}

			return roots_are_tuples_or_calls_to_this_function_and_compute_same_select_vector (node_p,node_defs,function_sdef_p,same_select_vector_p,has_tuple_tail_call_p);
		}
		case IfNode:
		{
			ArgP then_arg_p;
			NodeP else_node_p;

			then_arg_p=node_p->node_arguments->arg_next;

			if (!roots_are_tuples_or_calls_to_this_function_and_compute_same_select_vector (then_arg_p->arg_node,node_p->node_then_node_defs,function_sdef_p,same_select_vector_p,has_tuple_tail_call_p))
				return 0;

			else_node_p=then_arg_p->arg_next->arg_node;

			if (else_node_p->node_kind==NormalNode && else_node_p->node_symbol->symb_kind==fail_symb)
				return 1;

			return roots_are_tuples_or_calls_to_this_function_and_compute_same_select_vector (else_node_p,node_p->node_else_node_defs,function_sdef_p,same_select_vector_p,has_tuple_tail_call_p);
		}
		default:
		{
			if (node_p->node_kind==NormalNode){
				SymbolP node_symbol_p;

				node_symbol_p=node_p->node_symbol;
				if (node_symbol_p->symb_kind==tuple_symb){
					NodeIdP tuple_call_node_id_p;

					if (node_defs!=NULL && is_tuple_tail_call_modulo_cons_root (node_p,&tuple_call_node_id_p) &&
						is_tuple_tail_call_modulo_cons_defs (node_p,node_defs,tuple_call_node_id_p,same_select_vector_p))
					{
						*has_tuple_tail_call_p=1;
					}

					return 1;
				} else if (node_symbol_p->symb_kind==definition && node_symbol_p->symb_def==function_sdef_p
						&& node_p->node_arity==node_symbol_p->symb_def->sdef_arity)
					return 1;
			}
		}
	}

	return 0;
}

int is_tuple_tail_call_modulo_cons_root (NodeP root_node,NodeIdP *tuple_call_node_id_h)
{
	ArgP arg_p;
	int n;
	NodeIdP tuple_call_node_id_p;

	tuple_call_node_id_p=NULL;

	n=0;

	for_l (arg_p,root_node->node_arguments,arg_next){
		NodeP node_p;
		NodeIdP node_id_p;

		node_p=arg_p->arg_node;
		node_id_p=NULL;

		if (node_p->node_kind==FillUniqueNode)
			node_p=node_p->node_arguments->arg_node;

		if (node_p->node_kind==NodeIdNode && node_p->node_node_id->nid_refcount>0 && (node_p->node_node_id->nid_mark2 & NID_SELECTION_NODE_ID) &&
			node_p->node_node_id->nid_node->node_kind==NodeIdNode
		){
			/* tuple selector node changed to NodeIdNode in MarkTupleSelectorsNode */
			node_id_p=node_p->node_arguments->arg_node->node_node_id;
		} else if (node_p->node_kind==NormalNode){
			SymbolP symbol_p;

			symbol_p=node_p->node_symbol;
			if (symbol_p->symb_kind==select_symb){
				if (node_p->node_arguments->arg_node->node_kind==NodeIdNode && n+1==node_p->node_arity)
					node_id_p=node_p->node_arguments->arg_node->node_node_id;
			} else if ((symbol_p->symb_kind==cons_symb && node_p->node_arity==2) ||
						(symbol_p->symb_kind==definition && symbol_p->symb_def->sdef_kind==CONSTRUCTOR &&
						 node_p->node_arity==symbol_p->symb_def->sdef_arity))
			{
				ArgP constructor_arg_p;

				for_l (constructor_arg_p,node_p->node_arguments,arg_next){
					NodeP constructor_arg_node_p;

					constructor_arg_node_p=constructor_arg_p->arg_node;
					if (constructor_arg_node_p->node_kind==NodeIdNode){
						if (constructor_arg_node_p->node_node_id->nid_refcount>0
							&& (constructor_arg_node_p->node_node_id->nid_mark2 & NID_SELECTION_NODE_ID)
							&&  constructor_arg_node_p->node_node_id->nid_node->node_kind==NodeIdNode
						){
							if (node_id_p==NULL)
								/* tuple selector node changed to NodeIdNode in MarkTupleSelectorsNode */
								node_id_p=constructor_arg_node_p->node_arguments->arg_node->node_node_id;
							else {
								node_id_p=NULL;
								break;
							}
						}
					} else if (constructor_arg_node_p->node_kind==NormalNode && constructor_arg_node_p->node_symbol->symb_kind==select_symb
							&& constructor_arg_node_p->node_arguments->arg_node->node_kind==NodeIdNode && n+1==constructor_arg_node_p->node_arity
							&& node_id_p==NULL)
					{
						node_id_p=constructor_arg_node_p->node_arguments->arg_node->node_node_id;
					} else {
						node_id_p=NULL;
						break;
					}
				}
			}
		}

		if (node_id_p==NULL)
			break;

		if (tuple_call_node_id_p!=NULL){
			if (node_id_p!=tuple_call_node_id_p)
				break;
		} else
			tuple_call_node_id_p=node_id_p;

		++n;
	}

	*tuple_call_node_id_h=tuple_call_node_id_p;

	return (arg_p==NULL && tuple_call_node_id_p!=NULL && tuple_call_node_id_p->nid_refcount==root_node->node_arity
		&& tuple_call_node_id_p->nid_node->node_kind==NormalNode
		&& tuple_call_node_id_p->nid_node->node_symbol->symb_kind==definition
		&& tuple_call_node_id_p->nid_node->node_symbol->symb_def==CurrentSymbDef);
}

#endif
