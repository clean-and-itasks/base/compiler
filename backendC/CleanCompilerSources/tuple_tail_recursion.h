
extern int roots_are_tuples_or_calls_to_this_function_and_compute_same_select_vector (NodeP node_p,NodeDefP node_defs,SymbDef function_sdef_p,unsigned long *same_select_vector_p,int *has_tuple_tail_call_p);
extern int is_tuple_tail_call_modulo_cons_root (NodeP root_node,NodeIdP *tuple_call_node_id_h);
